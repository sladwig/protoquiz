
//  QQQQQ  TTTTTTT YY   YY PPPPPP  EEEEEEE  SSSSS
// QQ   QQ   TTT   YY   YY PP   PP EE      SS
// QQ   QQ   TTT    YYYYY  PPPPPP  EEEEE    SSSSS
// QQ  QQ    TTT     YYY   PP      EE           SS
//  QQQQ Q   TTT     YYY   PP      EEEEEEE  SSSSS

// questionTypes describe the behaviour of the different question types,
// e.g. multiplechoice-single, multiplechoice-multiple, truefalse, etc.
// to add a new questionType just create a new questionType object
// analogous to the ones already created

// First a collection of function from which we later compose our questionTypes

// select Behaviour - how to select answers
const selectThisAnswer = (selectedAnswer) => selectedAnswer
const selectOrDeselectThisAnswer = (selectedAnswer, currentAnswer) => {
  currentAnswer.has(selectedAnswer) ? currentAnswer.delete(selectedAnswer) : currentAnswer.add(selectedAnswer)

  return currentAnswer
}

// validation Behaviour - how to validate answers
const validateOnePossibleAnswer = (question, answer) => question.correct_answer === answer
const validateMultipleCorrectAnswers = (question, answers) => {
  // no answer -> can't be correct
  if (!answers) return false;

  // if not same length -> can't be correct then, too
  if (question.correct_answer.length !== answers.size) return false;

  // check if every correct answer has been selected
  for (let i = 0; i < question.length; i++) {
    if (!answers.has(question.correct_answer[i])) return false;
  }

  return true;
}

// possible Answers behaviour
const possibleAnswersOfQuestion = (question) => question.possible_answers
const possibleAnswersTrueAndFalse = () => [
  {caption: "Yes! Nothing but the truth!", a_id: true},
  {caption: "Nooo! Are you kidding me?", a_id: false}
]

// question behaviour
const titleOfQuestion = (question) => question.title
const titleTrueOrFalse = (question) => `${question.title}<br/>Is that true?`

// behaviour to determine if an answer is selected
const compareAnswer = (answer, selectedAnswer) => answer.a_id === selectedAnswer
const hasAnswer = (answer, selectedAnswer) => selectedAnswer.has(answer.a_id)
const includesAnswer = (answer, selectedAnswer) => selectedAnswer.includes(answer.a_id)

// behaviour to initialize the answer field
const undefinedAnswer = () => undefined
const emptySet = () => new Set()


// here the composition of questionTypes

// mutiplechoice-single
const mutiplechoiceSingle = {
  selectAnswer: selectThisAnswer,
  validate: validateOnePossibleAnswer,
  possibleAnswers: possibleAnswersOfQuestion,
  title: titleOfQuestion,
  isSelected: compareAnswer,
  isCorrect: compareAnswer,
  initializeAnswer: undefinedAnswer,
}

// mutiplechoice-multiple
const mutiplechoiceMultiple = {
  selectAnswer: selectOrDeselectThisAnswer,
  validate: validateMultipleCorrectAnswers,
  title: titleOfQuestion,
  possibleAnswers: possibleAnswersOfQuestion,
  isSelected: hasAnswer,
  isCorrect: includesAnswer,
  initializeAnswer: emptySet,
}

// truefalse
const truefalse = {
  selectAnswer: selectThisAnswer,
  validate: validateOnePossibleAnswer,
  title: titleTrueOrFalse,
  possibleAnswers: possibleAnswersTrueAndFalse,
  isSelected: compareAnswer,
  isCorrect: compareAnswer,
  initializeAnswer: undefinedAnswer,
}



// for easy access add questionTypes here
const questionTypes = {
  "mutiplechoice-single": mutiplechoiceSingle,
  "mutiplechoice-multiple": mutiplechoiceMultiple,
  "truefalse": truefalse,
}




//   AAA    CCCCC  TTTTTTT IIIII  OOOOO  NN   NN  SSSSS
//  AAAAA  CC    C   TTT    III  OO   OO NNN  NN SS
// AA   AA CC        TTT    III  OO   OO NN N NN  SSSSS
// AAAAAAA CC    C   TTT    III  OO   OO NN  NNN      SS
// AA   AA  CCCCC    TTT   IIIII  OOOO0  NN   NN  SSSSS

// actions - describing what is happening with the state

const LOAD_QUIZ = "LOAD_QUIZ";
function loadQuiz(quiz) {
  return {
    type: LOAD_QUIZ,
    quiz
  };
}

const LOAD_RESULT = "LOAD_RESULT";
function loadResult(result) {
  return {
    type: LOAD_RESULT,
    result
  };
}

const NEXT_QUESTION = "NEXT_QUESTION";
function nextQuestion(result) {
  return {
    type: NEXT_QUESTION
  };
}


const SELECT_ANSWER = "SELECT_ANSWER";
function selectAnswer(selectedAnswer) {
  return {
    type: SELECT_ANSWER,
    selectedAnswer
  };
}

const VALIDATE_QUESTION = "VALIDATE_QUESTION";
function validateQuestion() {
  return {
    type: VALIDATE_QUESTION
  };
}




// RRRRRR  EEEEEEE DDDDD   UU   UU  CCCCC  EEEEEEE RRRRRR
// RR   RR EE      DD  DD  UU   UU CC    C EE      RR   RR
// RRRRRR  EEEEE   DD   DD UU   UU CC      EEEEE   RRRRRR
// RR  RR  EE      DD   DD UU   UU CC    C EE      RR  RR
// RR   RR EEEEEEE DDDDDD   UUUUU   CCCCC  EEEEEEE RR   RR

// reducer - how state changes

function reducer(state, action) {

  // for convenience (and saner code) we have an `actual` object in the state,
  // which holds all information regarding the actual screen (question or results)
  // it gets initialized with an index and fills other attributes from it
  const getActualFrom = (index, quiz) => {
    if (index >= quiz.questions.length) {
      let {userScore, possibleScore} = quiz;
      return { done: true, calculatedResult: percentageOf(userScore, possibleScore) };
    }

    let question = quiz.questions[index]
    let questionType = questionTypes[question.question_type]
    return { index, question, questionType, answer: questionType.initializeAnswer() };
  }

  switch (action.type) {

    case LOAD_QUIZ: {
      // load quiz
      let quiz = {...action.quiz}

      // set user score and calculate possible Score
      quiz.userScore = 0
      quiz.possibleScore = quiz.questions.map((_) => _.points)
                                        .reduce((sum,x) => sum+x)

      // start with first question
      let actual = getActualFrom(0, quiz)

      return {...state, quiz, actual}
    }


    case LOAD_RESULT: {
      // load result
      let result = {...action.result}

      return {...state, result}
    }


    case NEXT_QUESTION: {
      const quiz = {...state.quiz}
      let nextIndex = state.actual.index + 1

      // create new actual object
      let actual = getActualFrom(nextIndex, quiz)

      return {...state, actual}
    }


    case SELECT_ANSWER: {
      // avoid selection during presentation of answers
      if (state.actual.showAnswer) return state

      let actual = {...state.actual}

      actual.answer = actual.questionType.selectAnswer(action.selectedAnswer, actual.answer)

      return {...state, actual}
    }


    case VALIDATE_QUESTION: {
      // avoid double validation
      if (state.actual.showAnswer) return state

      let {actual, quiz} = state

      // validate question
      const correct = actual.questionType.validate(actual.question, actual.answer)

      // increment user score if correct answer
      if (correct) quiz.userScore += actual.question.points

      actual.showAnswer = true
      actual.correct = correct

      return {...state, quiz, actual}
    }


    default:
      return state;
  }
}




//  CCCCC   OOOOO  MM    MM PPPPPP   OOOOO  NN   NN EEEEEEE NN   NN TTTTTTT  SSSSS
// CC    C OO   OO MMM  MMM PP   PP OO   OO NNN  NN EE      NNN  NN   TTT   SS
// CC      OO   OO MM MM MM PPPPPP  OO   OO NN N NN EEEEE   NN N NN   TTT    SSSSS
// CC    C OO   OO MM    MM PP      OO   OO NN  NNN EE      NN  NNN   TTT        SS
//  CCCCC   OOOO0  MM    MM PP       OOOO0  NN   NN EEEEEEE NN   NN   TTT    SSSSS

// how state gets represented in DOM

function Quiz(store) {
  let {quiz, result, actual} = store

  return  `<div id="quiz">
            ${(actual.done) ?
              Result(actual.calculatedResult, result.results) :
              Question(actual)
            }
          </div>`;
}

function checkAnswerAndContinue() {
  // validate question
  dispatchAction(validateQuestion)
  // start timer for next question
  window.setTimeout(()=> dispatchAction(nextQuestion), 3000)
}


function CheckButton(actual) {
  let text = ''
  let onClick = ''
  if (actual.showAnswer) {
    text = (actual.correct) ? "Well done!" : "Come on, you can do better!"
  } else {
    text = "check my answer"
    onClick = 'onclick="checkAnswerAndContinue()"'
  }
  return `<div class="continue" ${onClick}>
            <span>${text}</span>
          </div>`;
}

function Question(actual) {
  let possibleAnswers = actual.questionType
    .possibleAnswers(actual.question)
    .map((answer) => Answer(answer, actual))
    .join('');

  return  `<div class="question" style="background-image: url('${actual.question.img}');">
            <div class='text'><span>${actual.questionType.title(actual.question)}</span></div>
            ${possibleAnswers}
            ${CheckButton(actual)}
          </div>`;
}

function Answer(answer, actual) {
  let selected = actual.questionType.isSelected(answer, actual.answer);
  let correct = actual.showAnswer ? actual.questionType.isCorrect(answer, actual.question.correct_answer) : false

  return `<div class='answer${selected? ' selected' : ''}${correct? ' correct' : ''}'
               onClick='dispatchAction(selectAnswer, ${answer.a_id})'>
            <span>${answer.caption}</span>
          </div>`;
}

function Result(calculatedResult, results) {
  let finalResult = results.find((result) => result.minpoints <= calculatedResult
                                          && calculatedResult <= result.maxpoints)

  return `<div class="question" style="background-image: url('${finalResult.img}');">
            <div class='text result'>
              <span>Your reached ${calculatedResult}% - ${finalResult.title}<br/>${finalResult.message}</span>
            </div>
          </div>`;
}



//  OOOOO  TTTTTTT HH   HH EEEEEEE RRRRRR
// OO   OO   TTT   HH   HH EE      RR   RR
// OO   OO   TTT   HHHHHHH EEEEE   RRRRRR
// OO   OO   TTT   HH   HH EE      RR  RR
//  OOOO0    TTT   HH   HH EEEEEEE RR   RR

// will render our Quiz component into app div
function render() {
  let $div = document.getElementById('app');
  $div.innerHTML = Quiz(store);
}

// dispatch all actions to the reducer
document.addEventListener('action', function(e) {
  store = reducer(store, e.detail);
  document.dispatchEvent(new CustomEvent('state'));
}, false);

// rerender if state changes
document.addEventListener('state', render);

// dispatch an action
dispatchAction = function(detail, ...rest) {
  document.dispatchEvent(
    new CustomEvent('action', { detail: detail(...rest)})
  );
}

// calculate percentage
const percentageOf = (value, total) => (total === 0) ? 0 :  Math.floor(value/total *100)

// initialize store
let store = {};

const startQuiz = () => {
  return fetch('https://proto.io/en/jobs/candidate-questions/quiz.json')
    .then((response) => response.json())
    .then((json) => dispatchAction(loadQuiz, json))
    // after the quiz we can load the final score message already,
    // so the user doesn't have to wait for later or
    // potentially doesn't have an internet connection anymore or whatever ;)
    // here would also be a good place for error handling regarding the requests
    .then(() => fetch('https://proto.io/en/jobs/candidate-questions/result.json'))
    .then((response) => response.json())
    .then((json) => dispatchAction(loadResult, json))
}

startQuiz()




// simple logging for nice development, just uncomment following lines
// document.addEventListener('action', function(e) {
//   console.log("action", e.detail);
// });

// document.addEventListener('state', function(e) {
//   console.log("store changed", store);
// });
